using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horse : BulletTarget
{
    AttackController attackController;

    Waggon waggon;

    Animator animator;

    const string ANIM_PARAM_DEATH = "death";

    int hp = 100;

    Rider rider;

    float currentX = 0f;
    float currentZ = 0f;
    int numInLine = 0;

    List<HorsePart> horseParts;

    private void Awake()
    {
        horseParts = new List<HorsePart>();
        foreach (HorsePart horsePart in GetComponentsInChildren<HorsePart>())
        {
            horsePart.SetHorse(this);
            horseParts.Add(horsePart);
        }

        Vector3 nextPosition = transform.position;
        nextPosition.x += Random.Range(-Const.AREA_EDGE / 4f, Const.AREA_EDGE / 4f);
        transform.position = nextPosition;

        currentX = transform.position.x;
        waggon = FindObjectOfType<Waggon>();
        attackController = FindObjectOfType<AttackController>();
        animator = GetComponent<Animator>();
        rider = GetComponentInChildren<Rider>();
        rider.SetHorse(this);
    }

    public override void OnHit(Vector3 hitDirection, int hitDamage)
    {
        hp = 0;
        animator.SetTrigger(ANIM_PARAM_DEATH);
        if (rider.IsAlive())
        {
            rider.Boom();
        }

        StartCoroutine(Delete(10f));
    }

    IEnumerator Delete(float delay)
    {
        yield return new WaitForSeconds(delay);
        attackController.IAmDestroyed(this);
        Destroy(gameObject);
    }

    public bool IsAlive()
    {
        return hp > 0;
    }

    public void SetNumInLine(int numInLine)
    {
        this.numInLine = numInLine;
    }

    private void LateUpdate()
    {
        if (!IsAlive())
        {
            return;
        }

        float addSpeed = 0f;

        if (!IsNumInLine())
        {
            addSpeed = 12f;
        }

        Vector3 nextPosition = transform.position;
        nextPosition.z += (Const.HORSE_SPEED + addSpeed) * Time.deltaTime;
        transform.position = nextPosition;
    }

    public void OnRiderDead()
    {

    }

    public Rider GetRider()
    {
        return rider;
    }

    public bool IsNumInLine()
    {
        float pointZ = waggon.transform.position.z - (numInLine + 2) * Const.AREA_EDGE;
        float delta = transform.position.z - pointZ;
        return Mathf.Abs(delta) < 4f;
    }
}
