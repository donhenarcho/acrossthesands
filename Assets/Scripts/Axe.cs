using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : ThrowObject
{
    float damageDistance = 2f;

    Hero hero;

    private void Awake()
    {
        timer = 10f;
        moveSpeed = Const.AXE_SPEED;
        rotateSpeed = -20f;
        hero = FindObjectOfType<Hero>();
        transform.localRotation = Quaternion.Euler(0f, 0f, -90f);
    }

    void Update()
    {
        if (isThrown)
        {
            Vector3 newPosition = transform.position;
            newPosition += shotDirection * moveSpeed * Time.deltaTime;
            transform.position = newPosition;
            transform.RotateAroundLocal(Vector3.up, rotateSpeed * Time.deltaTime);

            if (Vector3.Distance(hero.GetHeadWorldPosition(), transform.position) < damageDistance)
            {
                hero.OnHit(shotDirection, GetDamage());
            }

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                StartCoroutine(Die(0f));
            }
        }
    }
}
