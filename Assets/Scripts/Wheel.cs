using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    const float speed = 16f;

    bool isMoving = true;

    public void Move(bool isMoving)
    {
        this.isMoving = isMoving;
    }

    private void LateUpdate()
    {
        if (isMoving)
        {
            transform.RotateAroundLocal(Vector3.back, speed * Time.deltaTime);
        }
    }
}
