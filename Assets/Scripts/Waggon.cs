using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waggon : MonoBehaviour
{
    private void LateUpdate()
    {
        Vector3 newPosition = transform.position;
        newPosition.z += Time.deltaTime * Const.HORSE_SPEED;
        transform.position = newPosition;
    }
}
