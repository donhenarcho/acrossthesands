using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    protected int damage = 0;
    protected Vector3 shotDirection;
    protected bool isThrown = false;
    protected float timer = 0f;
    protected float moveSpeed = 0f;
    protected float rotateSpeed = 0f;

    public int GetDamage()
    {
        return damage;
    }

    public Vector3 GetShotDirection()
    {
        return shotDirection;
    }

    public void Throw(Vector3 shotDirection, int damage)
    {
        this.shotDirection = shotDirection;
        this.damage = damage;
        isThrown = true;
    }

    protected IEnumerator Die(float delay)
    {
        yield return new WaitForSeconds(delay);

        isThrown = false;

        foreach (Renderer r1 in GetComponentsInChildren<Renderer>())
        {
            r1.enabled = false;
        }
        Renderer r2 = GetComponent<Renderer>();
        if (r2 != null)
        {
            r2.enabled = false;
        }

        Destroy(gameObject);
    }
}
