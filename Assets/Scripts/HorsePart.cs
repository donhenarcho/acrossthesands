using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorsePart : BulletTarget
{
    Horse horse;

    public void SetHorse(Horse horse)
    {
        this.horse = horse;
    }

    public override void OnHit(Vector3 hitDirection, int hitDamage)
    {
        horse.OnHit(hitDirection, hitDamage);
    }
}
