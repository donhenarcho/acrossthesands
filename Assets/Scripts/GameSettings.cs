﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    static GameSettings instance = null;

    enum BodySide
    {
        RIGHT,
        LEFT
    }

    [SerializeField] BodySide Side = BodySide.RIGHT;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public static GameSettings GetInstance()
    {
        return instance;
    }

    public bool IsRight()
    {
        return Side == BodySide.RIGHT;
    }
}
