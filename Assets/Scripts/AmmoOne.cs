﻿using UnityEngine;
using System.Collections;
using UnityEngine.XR.Interaction.Toolkit;
using System;

public class AmmoOne : MonoBehaviour
{
    [SerializeField] Gun.GunType[] forGunTypes;
    [SerializeField] GameObject Bullet;

    XRGrabInteractable xRGrabInteractable;

    Vector3 startLocalPositon;
    Quaternion startLocalQuaternion;

    bool inHand = false;

    AmmoEnter currentAmmoEnter;

    Vector3 localLeftPosition = new Vector3(0.056f, -0.038f, 0.01f);
    Quaternion localLeftRotation = Quaternion.Euler(0f, 90f, 0f);
    Vector3 localRightPosition = new Vector3(-0.056f, -0.038f, 0.01f);
    Quaternion localRightRotation = Quaternion.Euler(0f, -90f, 0f);

    MeshRenderer meshRenderer;

    AmmoBox ammoBox;

    private void Awake()
    {
        ammoBox = transform.parent.GetComponent<AmmoBox>();
        xRGrabInteractable = GetComponent<XRGrabInteractable>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        meshRenderer.enabled = false;
    }

    private void Start()
    {
        startLocalPositon = transform.localPosition;
        startLocalQuaternion = transform.localRotation;
    }

    private void OnEnable()
    {
        xRGrabInteractable.selectEntered.AddListener(OnSelectEntered);
        xRGrabInteractable.selectExited.AddListener(OnSelectExited);
    }

    private void OnDisable()
    {
        xRGrabInteractable.selectExited.RemoveListener(OnSelectExited);
    }

    private void OnSelectEntered(SelectEnterEventArgs arg0)
    {
        meshRenderer.enabled = true;
        if (arg0.interactor.name.Equals(Const.INTERACTOR_NAME_LEFT_HAND)) {
            Bullet.transform.localPosition = localLeftPosition;
            Bullet.transform.localRotation = localLeftRotation;
        }
        else
        {
            Bullet.transform.localPosition = localRightPosition;
            Bullet.transform.localRotation = localRightRotation;

        }
        inHand = true;
        ammoBox.Del();
    }

    private void OnSelectExited(SelectExitEventArgs arg0)
    {
        meshRenderer.enabled = false;
        inHand = false;

        if (currentAmmoEnter != null)
        {
            if (currentAmmoEnter.AddOne())
            {
                ammoBox.OnInGun();
            }
            else
            {
                ammoBox.Add();
            }
        }
        else
        {
            ammoBox.Add();
        }

        transform.localPosition = startLocalPositon;
        transform.localRotation = startLocalQuaternion;
        currentAmmoEnter = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!inHand)
            return;

        AmmoEnter ammoEnter = collision.gameObject.GetComponent<AmmoEnter>();
        if (ammoEnter != null)
        {
            currentAmmoEnter = ammoEnter;
            currentAmmoEnter.OmAmmoNearby(true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!inHand)
            return;

        AmmoEnter ammoEnter = collision.gameObject.GetComponent<AmmoEnter>();
        if (ammoEnter != null && currentAmmoEnter != null)
        {
            currentAmmoEnter.OmAmmoNearby(false);
            currentAmmoEnter = null;
        }
    }
}
