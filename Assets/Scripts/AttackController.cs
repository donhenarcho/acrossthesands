using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttackController : MonoBehaviour
{
    [SerializeField] GameObject[] prefabs;

    Waggon waggon;

    const int maxEnemyInOneLine = 4;
    const int numLines = 6;
    const int waveSize = 4;

    List<Horse> horses = new List<Horse>();
    Dictionary<int, List<Horse>> area = new Dictionary<int, List<Horse>>();
    Dictionary<Horse, int> horseInLine = new Dictionary<Horse, int>();

    float delayInDistance = 100f;
    bool addWavePlease = false;

    int[] randomLines;

    const float attackDelay = 10f;
    float attackTimer = 0f;

    private void Awake()
    {
        waggon = FindObjectOfType<Waggon>();
        InitArea();
        addWavePlease = true;
    }

    void InitArea()
    {
        for (int i = 0; i < numLines; i++)
        {
            area[i] = new List<Horse>();
        }

        randomLines = new int[numLines];
        for (int i = 0; i < numLines; i++)
        {
            randomLines[i] = i;
        }
    }

    private void Update()
    {
        if (addWavePlease)
        {
            addWavePlease = false;
            addWave();
        }

        attackTimer += Time.deltaTime;
        if (attackTimer >= attackDelay)
        {
            attackTimer = 0f;
            Attack();
        }
    }

    void Attack()
    {
        Horse horse = horses[Random.Range(0, horses.Count)];

        if (!horse.IsNumInLine())
        {
            return;
        }

        Rider rider = horse.GetRider();
        if (rider != null && rider.IsAlive())
        {
            rider.ReadyToAttack();
        }
    }

    int GetBestLine()
    {
        int bestLine1 = -1;
        int bestLine2 = -1;
        int bestValue1 = 100;
        int bestValue2 = 100;

        Utils.Shuffle(randomLines);

        for (int i = 0; i < numLines; i++)
        {
            int line = randomLines[i];
            int count = area[line].Count;

            if (count >= maxEnemyInOneLine)
            {
                continue;
            }

            if (bestValue1 > count)
            {
                if (bestValue2 > bestValue1)
                {
                    bestValue2 = bestValue1;
                    bestLine2 = bestLine1;
                }

                bestValue1 = count;
                bestLine1 = line;
            }
        }

        if (bestLine1 == -1)
        {
            return -1;
        }

        if (bestLine2 == -1)
        {
            return bestLine1;
        }

        if (Random.Range(0, 2) == 1)
        {
            return bestLine1;
        }

        return bestLine2;
    }

    void addWave()
    {
        float delay = 1f;
        for (int i = 0; i < waveSize; i++)
        {
            StartCoroutine(addOne(delay));
            delay += 1f;
        }
    }

    IEnumerator addOne(float delay)
    {
        yield return new WaitForSeconds(delay);
        int bestLine = GetBestLine();

        if (bestLine == -1)
        {
            yield break;
        }

        respawn(0, bestLine);
    }

    void respawn(int index, int line)
    {
        float resultX = (line - (int)(numLines / 2f)) * Const.AREA_EDGE + Const.AREA_EDGE / 2f;

        GameObject gameObject = Instantiate(prefabs[index], new Vector3(resultX, 0f, -delayInDistance), Quaternion.identity);
        Horse horse = gameObject.GetComponentInChildren<Horse>();
        horse.SetNumInLine(area[line].Count);
        horses.Add(horse);
        area[line].Add(horse);
        horseInLine.Add(horse, line);
    }

    public void IAmDestroyed(Horse horse)
    {
        horses.Remove(horse);
        area[horseInLine[horse]].Remove(horse);
        horseInLine.Remove(horse);

        resalculateNumsInLines();

        if (horses.Count == 0)
        {
            addWavePlease = true;
        }
    }

    void resalculateNumsInLines()
    {
        foreach(KeyValuePair<int, List<Horse>> keyValuePair in area)
        {
            int num = 0;
            foreach(Horse horse in keyValuePair.Value)
            {
                horse.SetNumInLine(num);
                num++;
            }
        }
    }
}
