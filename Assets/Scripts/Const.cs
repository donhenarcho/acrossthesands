using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Const
{
    public const float AREA_EDGE = 10f;
    public const float HORSE_SPEED = 16f; // in m/s
    public const float BULLET_SPEED = 270f; // in m/s
    public const float AXE_SPEED = 40f; // in m/s

    public const int LAYER_AMMO_ENTER = 9;

    public const float REVOLEVER_HIT_RADIUS = 2f;
    public const float REVOLEVER_HIT_DISTANCE = 100f;


    public const string INTERACTOR_NAME_LEFT_HAND = "LeftHand";
    public const string INTERACTOR_NAME_RIGHT_HAND = "RightHand";
}
