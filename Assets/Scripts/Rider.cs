using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rider : MonoBehaviour
{
    [SerializeField] GameObject WeaponInHand;
    [SerializeField] GameObject ThrowPrefab;

    Animator animator;
    List<ManPart> manParts;
    Rigidbody rbBodyCenter;

    Horse horse;

    const string ANIM_PARAM_EQUIP = "equip";
    const string ANIM_PARAM_ATTACK = "attack";

    Hero hero;

    int hp = 100;

    float damageFactor = 100f;

    bool readyToAttack = false;

    float attackTimer = 0f;
    const float attackDelay = 8f;

    public void SetHorse(Horse horse)
    {
        this.horse = horse;
    }

    private void Awake()
    {
        WeaponInHand.SetActive(false);

        hero = FindObjectOfType<Hero>();
        animator = GetComponent<Animator>();
        manParts = new List<ManPart>();
        foreach (ManPart manPart in GetComponentsInChildren<ManPart>())
        {
            manPart.SetMan(this);
            manParts.Add(manPart);
        }
    }

    private void Start()
    {
        foreach (ManPart manPart in manParts)
        {
            if (manPart.IsBodyCenter())
            {
                rbBodyCenter = manPart.GetRb();
            }
        }
    }

    private void Update()
    {
        if (!IsAlive())
        {
            return;
        }

        if (!readyToAttack)
        {
            return;
        }

        readyToAttack = false;
        Attack();
    }

    void Attack()
    {
        animator.SetTrigger(ANIM_PARAM_EQUIP);
        StartCoroutine(AttackStep1());
        StartCoroutine(AttackStep2());
        StartCoroutine(AttackStep3());
    }

    IEnumerator AttackStep1()
    {
        yield return new WaitForSeconds(1.0f);
        WeaponInHand.SetActive(true);
    }

    IEnumerator AttackStep2()
    {
        yield return new WaitForSeconds(1.8f);
        animator.SetTrigger(ANIM_PARAM_ATTACK);
    }

    IEnumerator AttackStep3()
    {
        yield return new WaitForSeconds(2.8f);
        WeaponInHand.SetActive(false);
        GameObject gameObject = Instantiate(ThrowPrefab, WeaponInHand.transform.position, Quaternion.identity);

        Vector3 headPosition = hero.GetHeadWorldPosition();
        float distance = Vector3.Distance(headPosition, WeaponInHand.transform.position);
        Vector3 direction = (headPosition + new Vector3(0f, 0f, Const.HORSE_SPEED * 1f) - WeaponInHand.transform.position).normalized;

        DebugController.GetInstance().Log(distance.ToString() + " | " + (distance / Const.AXE_SPEED).ToString());
        gameObject.GetComponent<ThrowObject>().Throw(direction, 100);
    }

    public void OnHit(Vector3 hitDirection, int hitDamage, ManPart manPartTarget)
    {
        DeathPrepare();
        manPartTarget.GetRb().AddForce(hitDirection * hitDamage * damageFactor);
        StartCoroutine(Delete(10f));
    }

    IEnumerator Delete(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }

    public bool IsAlive()
    {
        return hp > 0;
    }

    Vector3 GetDirectionToHero()
    {
        return (hero.GetHeadPosition() - transform.position).normalized;
    }

    void DeathPrepare()
    {
        horse.OnRiderDead();
        transform.SetParent(null);
        hp = 0;
        animator.enabled = false;
        foreach (ManPart manPart in manParts)
        {
            manPart.GetRb().velocity = Vector3.zero;
            manPart.RbOn();
        }
    }

    public void Boom()
    {
        DeathPrepare();

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0, 360), transform.eulerAngles.z);
        float speed = 10000f;
        rbBodyCenter.isKinematic = false;
        Vector3 force = transform.forward;
        force = new Vector3(force.x, 1, force.z);
        rbBodyCenter.AddForce(force * speed);

        rbBodyCenter.AddTorque(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) * Random.Range(300, 600));

        StartCoroutine(Delete(5f));
    }

    public void ReadyToAttack()
    {
        readyToAttack = true;
    }
}
