using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI simpleUIText;
    static DebugController instance = null;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void Log(string message)
    {
        if (simpleUIText != null)
        {
            simpleUIText.text = message;
        }
        Debug.Log(message);
    }

    public void Log(string message, float delay)
    {
        if (simpleUIText != null)
        {
            simpleUIText.text = message;
        }
        StartCoroutine(clear(delay));
    }

    IEnumerator clear(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (simpleUIText != null)
        {
            simpleUIText.text = "";
        }
    }

    public static DebugController GetInstance()
    {
        return instance;
    }
}
