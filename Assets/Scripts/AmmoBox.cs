﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AmmoBox : MonoBehaviour
{
    [SerializeField] GameObject OnePrefab;
    [SerializeField] GameObject BulletsRoot;

    GameObject currentOne;

    int maxCount = 0;
    int currentCount = 0;

    List<GameObject> bullets;

    private void Awake()
    {
        bullets = new List<GameObject>();
        foreach (Transform bullet in BulletsRoot.transform)
        {
            maxCount++;
            bullets.Add(bullet.gameObject);

        }
        Reload();
    }

    void Respawn()
    {
        currentOne = Instantiate(OnePrefab, Vector3.zero, Quaternion.identity, transform);
        currentOne.transform.localPosition = Vector3.zero;
    }

    void UpdateBullets()
    {
        for (int i = 0; i < maxCount; i++)
        {
            bullets[i].SetActive(i < currentCount);
        }
    }

    public void Add()
    {
        if (currentCount >= maxCount)
        {
            currentCount = maxCount;
            return;
        }
        currentCount++;
        UpdateBullets();
    }

    public void Del()
    {
        if (currentCount <= 0)
        {
            currentCount = 0;
            return;
        }
        currentCount--;
        UpdateBullets();
    }

    public void OnInGun()
    {
        if (currentCount == 0)
        {
            Destroy(currentOne);
        }
    }

    public void Reload()
    {
        currentCount = maxCount;
        UpdateBullets();
        Respawn();
    }
}
