using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManPart : BulletTarget
{
    public enum EManPart
    {
        HEAD,
        BODY,
        LEFT_HAND,
        RIGHT_HAND,
        LEFT_LEG,
        RIGHT_LEG
    }

    [SerializeField] EManPart eManPart;
    [SerializeField] bool bodyCenter;

    Rider man;
    Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
    }

    public void SetMan(Rider man)
    {
        this.man = man;
    }

    public override void OnHit(Vector3 hitDirection, int hitDamage)
    {
        man.OnHit(hitDirection, hitDamage, this);
    }

    public void RbOn()
    {
        rb.useGravity = true;
    }

    public Rigidbody GetRb()
    {
        return rb;
    }

    public bool IsBodyCenter()
    {
        return bodyCenter;
    }
}
