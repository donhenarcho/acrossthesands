using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Hero : BulletTarget
{
    [SerializeField] HeroBody heroBody;
    XRRig xRRig;

    private void Awake()
    {
        heroBody.SetHero(this);

        xRRig = GetComponent<XRRig>();
    }

    private void FixedUpdate()
    {
        heroBody.transform.localPosition = GetHeadPosition();

        Vector3 rotation = GetHeadRotation().ToEulerAngles();
        //DebugController.GetInstance().Log(rotation.ToString());
        if (rotation.x < 0.4f)
        {
            heroBody.transform.localRotation = Quaternion.EulerAngles(0f, rotation.y, 0f);
        }
    }

    public override void OnHit(Vector3 hitDirection, int hitDamage)
    {
        DebugController.GetInstance().Log(">>> HERO = " + hitDamage, 2f);
    }

    public Vector3 GetHeadPosition()
    {
        Vector3 headPosition = Vector3.zero;
        if (XRSettings.enabled)
        {
            InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.Head);
            if (device.isValid)
            {
                device.TryGetFeatureValue(CommonUsages.centerEyePosition, out headPosition);
            }
        }
        else
        {
            headPosition = Camera.main.transform.position;
        }
        return headPosition;
    }

    public Vector3 GetHeadWorldPosition()
    {
        Vector3 headPosition = Vector3.zero;
        if (XRSettings.enabled)
        {
            InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.Head);
            if (device.isValid)
            {
                device.TryGetFeatureValue(CommonUsages.centerEyePosition, out headPosition);
            }
        }
        else
        {
            headPosition = Camera.main.transform.position;
        }
        return transform.TransformPoint(headPosition);
    }

    public Quaternion GetHeadRotation()
    {
        Quaternion headRotation = Quaternion.identity;
        if (XRSettings.enabled)
        {
            InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.Head);
            if (device.isValid)
            {
                device.TryGetFeatureValue(CommonUsages.centerEyeRotation, out headRotation);
            }
        }
        else
        {
            headRotation = Camera.main.transform.rotation;
        }
        return headRotation;
    }
}
