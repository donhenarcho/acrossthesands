using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class Gun : MonoBehaviour
{
    [SerializeField] GunType gunType;
    [SerializeField] ParticleSystem FireSystem;
    [SerializeField] Transform BulletPoint;
    [SerializeField] int AmmoMax;
    [SerializeField] GameObject Guard;
    [SerializeField] GameObject Nab;
    [SerializeField] InputActionReference inputActionReference_RightTriggerValue;
    [SerializeField] InputActionReference inputActionReference_LeftTriggerValue;

    public enum GunType
    {
        REVOLVER,
        RIFLE,
        SHOTGUN
    }

    const float guardMaxAngleX = 40f;
    const float nabMaxAngleX = -40f;

    AudioController audioController;

    XRGrabInteractable xRGrabInteractable;

    Vector3 startLocalPositon;
    Quaternion startLocalQuaternion;

    AmmoEnter ammoEnter;
    int ammo;

    bool inHand = false;
    bool isRightHand = false;

    private void Awake()
    {
        ammo = AmmoMax;

        ammoEnter = GetComponentInChildren<AmmoEnter>();
        ammoEnter.SetGun(this);

        xRGrabInteractable = GetComponent<XRGrabInteractable>();
        audioController = GetComponent<AudioController>();

        startLocalPositon = transform.localPosition;
        startLocalQuaternion = transform.localRotation;
    }

    private void OnEnable()
    {
        xRGrabInteractable.activated.AddListener(OnActivated);
        xRGrabInteractable.deactivated.AddListener(OnDeactivated);
        xRGrabInteractable.selectEntered.AddListener(OnSelectEntered);
        xRGrabInteractable.selectExited.AddListener(OnSelectExited);

        inputActionReference_RightTriggerValue.action.performed += RightTriggerValue;
        inputActionReference_LeftTriggerValue.action.performed += LeftTriggerValue;
    }

    private void OnDisable()
    {
        xRGrabInteractable.activated.RemoveListener(OnActivated);
        xRGrabInteractable.deactivated.RemoveListener(OnDeactivated);
        xRGrabInteractable.selectEntered.RemoveListener(OnSelectEntered);
        xRGrabInteractable.selectExited.RemoveListener(OnSelectExited);

        inputActionReference_RightTriggerValue.action.performed -= RightTriggerValue;
        inputActionReference_LeftTriggerValue.action.performed -= LeftTriggerValue;
    }

    private void OnDeactivated(DeactivateEventArgs arg0)
    {
    }

    private void OnActivated(ActivateEventArgs arg0)
    {
        Shot();
    }

    private void OnSelectEntered(SelectEnterEventArgs arg0)
    {
        inHand = true;
        isRightHand = arg0.interactor.name.Equals(Const.INTERACTOR_NAME_RIGHT_HAND);
    }

    private void OnSelectExited(SelectExitEventArgs arg0)
    {
        inHand = false;
        transform.localPosition = startLocalPositon;
        transform.localRotation = startLocalQuaternion;
    }

    private void RightTriggerValue(InputAction.CallbackContext obj)
    {
        if (!inHand || !isRightHand)
        {
            return;
        }

        float value = obj.ReadValue<float>();

        Guard.transform.localRotation = Quaternion.Euler(guardMaxAngleX * value, 0f, 0f);
        Nab.transform.localRotation = Quaternion.Euler(nabMaxAngleX * value, 0f, 0f);
    }

    private void LeftTriggerValue(InputAction.CallbackContext obj)
    {
        if (!inHand || isRightHand)
        {
            return;
        }

        float value = obj.ReadValue<float>();

        Guard.transform.localRotation = Quaternion.Euler(guardMaxAngleX * value, 0f, 0f);
        Nab.transform.localRotation = Quaternion.Euler(nabMaxAngleX * value, 0f, 0f);
    }

    public void Shot()
    {
        StartCoroutine(ResetGunDetailsPositions());

        if (ammo <= 0)
        {
            ammoEnter.OnShot(false);
            ammo = 0;
            audioController.Play(1);
            return;
        }

        ammo -= 1;
        FireSystem.Play();
        audioController.Play(0);

        RaycastHit hit;
        if (Physics.SphereCast(BulletPoint.position, getHitRadius(), BulletPoint.TransformDirection(Vector3.forward), out hit, getHitDistance()))
        {
            DebugController.GetInstance().Log(hit.transform.gameObject.name, 1f);
            BulletTarget bulletTarget = hit.transform.gameObject.GetComponent<BulletTarget>();
            if (bulletTarget != null)
            {
                bulletTarget.OnHit(BulletPoint.TransformDirection(Vector3.forward), 100);
            }
        }
        else
        {
            DebugController.GetInstance().Log("mimo", 1f);
        }

        ammoEnter.OnShot(true);
    }

    public GunType GetGunType()
    {
        return gunType;
    }

    public bool AddAmmoOne()
    {
        if (ammo >= AmmoMax)
        {
            ammo = AmmoMax;
            return false;
        }

        ammo += 1;
        audioController.Play(2);
        return true;
    }

    float getHitRadius()
    {
        switch (gunType)
        {
            case GunType.REVOLVER:
                return Const.REVOLEVER_HIT_RADIUS;
            default:
                return Const.REVOLEVER_HIT_RADIUS;
        }
    }

    float getHitDistance()
    {
        switch (gunType)
        {
            case GunType.REVOLVER:
                return Const.REVOLEVER_HIT_DISTANCE;
            default:
                return Const.REVOLEVER_HIT_DISTANCE;
        }
    }

    public int GetAmmo()
    {
        return ammo;
    }

    public int GetAmmoMax()
    {
        return AmmoMax;
    }

    public bool InHand()
    {
        return inHand;
    }

    public bool IsRightHand()
    {
        return isRightHand;
    }

    IEnumerator ResetGunDetailsPositions()
    {
        yield return new WaitForSeconds(0.1f);
        Guard.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
        Nab.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }
}