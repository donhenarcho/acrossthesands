using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroBody : BulletTarget
{
    [SerializeField] GameObject RevolverRight;
    [SerializeField] GameObject AmmoBoxLeft;
    [SerializeField] GameObject RevolverLeft;
    [SerializeField] GameObject AmmoBoxRight;

    Hero hero;

    private void Awake()
    {
        RevolverRight.SetActive(false);
        AmmoBoxLeft.SetActive(false);
        RevolverLeft.SetActive(false);
        AmmoBoxRight.SetActive(false);
    }

    private void Start()
    {
        if (GameSettings.GetInstance().IsRight())
        {
            RevolverRight.SetActive(true);
            AmmoBoxLeft.SetActive(true);
        }
        else
        {
            RevolverLeft.SetActive(true);
            AmmoBoxRight.SetActive(true);
        }
    }

    public void SetHero(Hero hero)
    {
        this.hero = hero;
    }

    public override void OnHit(Vector3 hitDirection, int hitDamage)
    {
        hero.OnHit(hitDirection, hitDamage);
    }
}
