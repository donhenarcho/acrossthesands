using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestController : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] Transform respawnPoint;

    GameObject prefabObject;

    private void Awake()
    {
    }

    private void Start()
    {
        
    }

    private void LateUpdate()
    {
        if (prefabObject == null)
        {
            Respawn();
        }
    }

    void Respawn()
    {
        prefabObject = Instantiate(prefab, respawnPoint.position, Quaternion.identity);
        prefabObject.transform.Rotate(new Vector3(0f, 0f, 0f));
    }
}
