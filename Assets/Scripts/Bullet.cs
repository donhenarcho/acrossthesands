using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : ThrowObject
{
    private void Awake()
    {
        timer = 2f;
    }

    void Update()
    {
        if (isThrown)
        {
            Vector3 newPosition = transform.position;
            newPosition += shotDirection * Const.BULLET_SPEED * Time.deltaTime;
            transform.position = newPosition;
            this.transform.forward = shotDirection;

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                StartCoroutine(Die(0f));
            }
        }
    }
}
