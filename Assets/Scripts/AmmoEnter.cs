﻿using UnityEngine;
using System.Collections;

public class AmmoEnter : MonoBehaviour
{
    [SerializeField] GameObject[] Bullets;

    Gun gun;
    Vector3 startLocalPosition;
    Vector3 ammoNearbyPosition;

    int lastAmmo = -1;

    private void Awake()
    {
        gameObject.SetLayer(Const.LAYER_AMMO_ENTER);
    }

    private void Start()
    {
        startLocalPosition = transform.localPosition;
        ammoNearbyPosition = new Vector3(startLocalPosition.x - 0.02f, startLocalPosition.y, startLocalPosition.z);
    }

    public void SetGun(Gun gun)
    {
        this.gun = gun;
    }

    public bool AddOne()
    {
        bool result = gun.AddAmmoOne();
        if (result)
        {
            UpdateBullets();
        }
        StartCoroutine(Close());
        return result;
    }

    public void OmAmmoNearby(bool value)
    {
        UpdateBullets();

        ammoNearbyPosition = new Vector3(startLocalPosition.x + (gun.IsRightHand() ? -0.02f : 0.02f), startLocalPosition.y, startLocalPosition.z);
        transform.localPosition = value ? ammoNearbyPosition : startLocalPosition;
    }

    void UpdateBullets()
    {
        if (lastAmmo == gun.GetAmmo())
        {
            return;
        }

        lastAmmo = gun.GetAmmo();
        for (int i = 0; i < gun.GetAmmoMax(); i++)
        {
            Bullets[i].SetActive(i < lastAmmo);
        }
    }

    public void OnShot(bool result)
    {
        if (result)
        {
            UpdateBullets();
        }
        transform.RotateAroundLocal(Vector3.forward, 60f);
    }

    IEnumerator Close()
    {
        yield return new WaitForSeconds(0.2f);
        transform.localPosition = startLocalPosition;
    }
}
